from util import *

def solutionOne():
    """ Solution on paper no mathematical procedures """
    printSolution('1')

def solutionTwo():
    """ Problem 2
        Finding probability that 6th person choosen will have high blood
                pressure when expected value, X, is 12.5
        Geometric Distribution with p as 1/X
    """
    p = 1/12.5
    answer = (1-p)**5 * 5
    printSolution('2', answer)

def solutionThree():
    """ Problem 3
        Using the values for Mean and Variance solve.

        E[a+bx] = a + bE[x]
        Var[a+bx] = a^2(Var[x])

        E[x^2] = Var[x] + E[x]^2
    """
    printSolution('3')
    EX = 1
    VarX = 5
    EX2 = VarX + EX**2

    solutionThreeA(EX, EX2)
    solutionThreeB(VarX)

def solutionThreeA(EX, EX2):
    answer = 4 + 4*EX + EX2
    printSolution('3a', answer, True)

def solutionThreeB(VarX):
    answer = 9 * VarX
    printSolution('3b', answer, True)

def solutionFour():
    """ Problem 4
        Bernoulli Distribution

        Summation of nCr(N,n) * P**n * p**(N-n)
    """
    answer = ber(31, 32, .1, .9)
    printSolution('4', answer)

def solutionFive():
    """ Problem 5
        Poisson with rate multiplied by 5
        mean = 10
    """
    printSolution('5')
    solutionFiveA(0)
    solutionFiveB(4)

def solutionFiveA(x):
    answer = poi(x, 2*5)
    printSolution('5a', answer, True)

def solutionFiveB(x):
    answer = 0
    for i in range(x):
        answer += poi(i, 2*5)
    printSolution('5b', 1 - answer, True)

def solutionSix():
    """ Problem 6
        Bayes formula to determine how likely the new drug has helped a citizen.

        event A = getting two colds with drug being effective = poi(2,3)
        event B = drug being effective for a citizen = .75
        event B' = drug sucks for a citizen = .25
    """
    answer = bay(poi(2,3), poi(2,5), .75, .25)
    printSolution('6', answer)

def solutionSeven():
    """ Problem 7
        Finding the first two moments as well as the mean and variance given a
                m.g.f
        The first two moments of a m.g.f are mean and standard deviation

        mean is found by derivative of m(0)
        variance is found by m''(0) - [m'(0)]^2
    """
    def m(t):
        return e(3*t) / (1 - t**2)
    printSolution('7')
    dm = derivative(m)
    d2m = derivative(dm)
    print("\tFirst moment and mean is", end = " ")
    printSolution('7a', round(dm(0)))
    print("\tSecond moment is", end = " ")
    printSolution('7b', round(d2m(0)))
    print("\tStandard Deviation is", end = " ")
    radicand = round(d2m(0)) - round(dm(0)) ** 2
    printSolution('7c', math.sqrt(radicand))

def solutionEight():
    """ Find probability of 2 or 3 using given mgf

        M(t) = ((1/3) + (2/3)e^t)^5
        Binomial Distribution where
        N = 5
        n = 3
        P = 1/3
        p = 2/3
    """
    pA = binom(5, 2, (1/3), (2/3))
    pB = binom(5, 3, (1/3), (2/3))
    printSolution('8', (pA+pB))

def solutionNine():
    """ Problem 9
        Balls in an urn

        without replacement nCr * nCr / nCr
    """
    answer = (nCr(4,2) * nCr(6,1)) / nCr(10,3)
    printSolution('9', answer)

def solutionTen():
    """ Problem 10
        Negative binomial distribution

        xCr * P^r * (1-P)^(x-r)

        x = 5
        r = 3
        P = 1/3
    """
    answer = nbinom(5, 3, (1/3))
    printSolution('10', answer)

def main():
    solutionOne()
    solutionTwo()
    solutionThree()
    solutionFour()
    solutionFive()
    solutionSix()
    solutionSeven()
    solutionEight()
    solutionNine()
    solutionTen()

if __name__ == '__main__':
    main()
