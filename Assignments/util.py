import math

""" Homework 7 - Poisson Distribution """

""" Helper functions and variables """
f = math.factorial
e = math.exp

def nCr(n,r):
    """ Combination formula

    Args:
        n (int|float): choosing from
        r (int|float): chossing amount

    Returns:
        int|float
    """
    return f(n) / f(r) / f(n-r)

def poi(x,m):
    """ Poisson distribution

    Args:
        x (int|float): outcome
        m (int|float): mean

    Returns:
        int|float
    """
    return e(-m) * (m**x) / f(x)

def ber(start=0, end=1, pinc=.5, pexc=.5):
    answer = 0
    for x in range((end + 1))
        answer += nCr(end, start) * (pinc ** start) * (pexc ** (start - end))

def printSolution(solutionNumber='', answer = -1, tab=False):
    if (tab):
        if (answer >= 0):
            print("\tSolution to {0}: {1}"
                    .format(solutionNumber, format(answer, '.15f')))
        else:
            print("\tSolution to {0}".format(solutionNumber))
    else:
        if (answer >= 0):
            print("Solution to {0}: {1}"
                    .format(solutionNumber, format(answer, '.15f')))
        else:
            print("Solution to {0}".format(solutionNumber))
""" end Helper Functions """
