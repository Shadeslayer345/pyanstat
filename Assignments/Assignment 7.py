from util import *

def solutionOne():
    """ Using Poisson to determine probability of running out of beds at
            hospital

        mean: 1.5
        probability: > 8

        To find all probabilities greater than 8 we simply sum all less
                than 8 and subtract from 1.
    """
    answer = 0
    for x in range(9):
        answer += poi(x, 1.5)

    printSolution('1', 1 - answer)

def solutionTwo():
    """ In a randomly selected office hour, the number of student arrivals is 7

        mean: 2.5
        probability: 7

        Simple call to poi with correct params.
    """

    answer = poi(7, 2.5)
    printSolution('2', answer)

def solutionThree():
    """ Two typist with separate error means, find probability that you will
                have no errors.

        meanA: 3.2
        meanB: 3.7


        Find probability separately for each typist and average
    """

    answer = (poi(0, 3.2) + poi(0, 3.7)) / 2
    printSolution('3', answer)

def solutionFour():
    """ 3 Parts each dealing with accidents with a mean of 4.6 per week

        mean: 4.6
    """
    printSolution('4')
    solutionFourA(0)
    solutionFourB(9)
    solutionFourC(1)

def solutionFourA(x):
    """ No accidents

    Args:
        x (int): Probability to solve for.

    Simple poi using probability and mean
    """

    answer = poi(x, 4.6)
    printSolution('4a', answer, True)

def solutionFourB(x):
    """ 9 or more accidents

    Args:
        x (int): Probability to solve for.
    To find all probabilities greater than or equal to 9 we simply sum all less
            than 8 and subtract from 1.
    """

    answer = 0
    for x in range(9):
        answer += poi(x, 4.6)

    printSolution('4b', 1 - answer, True)

def solutionFourC(x):
    """ One accident today

    Args:
        x (int): Probability to solve for.

    One day is 1/7 of a week so subsitute that for 4.6 and solve.
    """

    answer = poi(1, 0.6571428571428571)
    printSolution('4c', answer, True)

def solutionFive():
    """ 2 parts each dealing with cars arriving at a gas station with a mean of
            9 per hour.
    """

    printSolution('5')
    solutionFiveA(1)
    solutionFiveB(25)

def solutionFiveA(x):
    """ One car in the next hour

    Args:
        x (int|float): Probability we are solving for.

    Simple poi with params and mean
    """
    answer = poi(x, 9)
    printSolution('5a', answer, True)

def solutionFiveB(x):
    """ Over 25 cars in the next 5 hours

    Args:
        x (int|float)

    Find all probabilities less than or equal to 25, using 45 as the mean.
    """
    newMean = 9 * 5
    answer = 0
    for i in range(x+1):
        answer += poi(i, newMean)
    printSolution('5b', 1 - answer, True)

def main():
    solutionOne()
    solutionTwo()
    solutionThree()
    solutionFour()
    solutionFive()


if __name__ == '__main__':
    main()
